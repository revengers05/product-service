package com.example.productService.repository;

import com.example.productService.Entity.ProductEntity;
import org.springframework.data.mongodb.repository.DeleteQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Repository
public interface IProductRepository extends MongoRepository<ProductEntity, String> {
    List<ProductEntity> findTop10ByOrderByQuantitySoldDesc();

    boolean existsByProductNameAndColorAndRam(String productName, String color, String ram);

    Optional<ProductEntity> findByProductNameAndColorAndRam(String productName, String color, String ram);

    List<ProductEntity> findByProductName(String productName);

}
