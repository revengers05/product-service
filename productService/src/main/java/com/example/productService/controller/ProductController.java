package com.example.productService.controller;

import com.example.productService.model.ProductModel;
import com.example.productService.model.ProductSearchRequest;
import com.example.productService.service.IProductService;
import com.example.productService.service.impl.ProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.security.PublicKey;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/v1.0/product")
public class ProductController {
    @Autowired
    IProductService productService;

    @GetMapping(value = "/allProduct")
    public List<ProductModel> getAllProduct() {
        log.info("Inside Controller to get all prodcuts");
        return productService.getAllProducts();
    }

    @GetMapping(value = "/getByName")
    public ResponseEntity<List<ProductModel>> getProductByName(@RequestParam String productName) {
        log.info("Got call to get by name: {}", productName);
        return productService.getProductByName(productName);
    }

    @GetMapping(value = "/getProductById")
    public ResponseEntity<ProductModel> getProductById(@RequestParam String productId) {
        log.info("Got call to get the products with id: {}", productId);
        ProductModel result = productService.getProductById(productId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/topProducts")
    public ResponseEntity<List<ProductModel>> getTopProduct() {
        log.info("Getting top most Sold Product");
        return productService.getTopToSellingProducts();
    }

    @PutMapping(value = "/updateStock")
    public void updateStockOfProduct(@RequestParam String productId, @RequestParam Integer count) {
        log.info("Got request to update the stock of product id: {}, by: {}", productId, count);
        productService.updateStockOfProduct(productId, count);
    }

    @DeleteMapping(value = "/deleteProduct")
    public void deleteProduct(@RequestParam String productId) {
        log.info("Got request to delete product with id: {}", productId);
        productService.deleteProductById(productId);
    }

    @PostMapping(value = "/addProduct")
    public ResponseEntity<String> addProduct(@RequestBody ProductModel product) {
        log.info("Got Request to add new product {}", product);
        return productService.addNewProduct(product);
    }

    @PostMapping("/checkProduct")
    public ResponseEntity<String> checkProduct(@RequestBody ProductSearchRequest productRequest) {
        log.info("Got call to check for product with name: {}, ram: {}, color: {}",
                productRequest.getProductName(), productRequest.getRam(), productRequest.getColor());
        return productService.checkProduct(productRequest);
    }
}
