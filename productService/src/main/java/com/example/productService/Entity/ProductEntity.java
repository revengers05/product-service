package com.example.productService.Entity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Slf4j
@Document(collection = "products")
public class ProductEntity {
    @Id
    private String id;

    @Field("productName")
    private String productName;

    @Field("imageUrl")
    private String imageUrl;

    @Field("usp")
    private String usp;

    @Field("ram")
    private String ram;

    @Field("color")
    private String color;

    @Field("quantitySold")
    private int quantitySold;

    @Field("totalStock")
    private int totalStock;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUsp() {
        return usp;
    }

    public void setUsp(String usp) {
        this.usp = usp;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(int quantitySold) {
        this.quantitySold = quantitySold;
    }

    public int getTotalStock() {
        return totalStock;
    }

    public void setTotalStock(int totalStock) {
        this.totalStock = totalStock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId='" + id + '\'' +
                ", productName='" + productName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", usp='" + usp + '\'' +
                ", ram='" + ram + '\'' +
                ", color='" + color + '\'' +
                ", quantitySold=" + quantitySold +
                ", totalStock=" + totalStock +
                '}';
    }
}
