package com.example.productService.service;

import com.example.productService.Entity.ProductEntity;
import com.example.productService.model.ProductModel;
import com.example.productService.model.ProductSearchRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IProductService {
    List<ProductModel> getAllProducts();
    ResponseEntity<List<ProductModel>> getTopToSellingProducts();
    ResponseEntity<List<ProductModel>> getProductByName(String productName);
    void updateStockOfProduct(String productId, Integer count);

    void deleteProductById(String productId);

    ResponseEntity<String> addNewProduct(ProductModel productModel);

    ResponseEntity<String> checkProduct(ProductSearchRequest productSearchRequest);

    ProductModel getProductById(String id);
}
