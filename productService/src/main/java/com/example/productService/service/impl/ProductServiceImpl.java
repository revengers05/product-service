package com.example.productService.service.impl;

import com.example.productService.Entity.ProductEntity;
import com.example.productService.model.ProductModel;
import com.example.productService.model.ProductSearchRequest;
import com.example.productService.repository.IProductRepository;
import com.example.productService.service.IProductService;
import com.mongodb.client.model.ReturnDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    IProductRepository productRepository;

    public List<ProductModel> getAllProducts() {
        log.info("Got call to get all products");
        List<ProductModel> productModelList = new ArrayList<>();
        try {
            List<ProductEntity> products = productRepository.findAll();
            if (products != null && !products.isEmpty()) {
                log.info("Got products from product repository: {}", products);
                for (ProductEntity product : products) {
                    ProductModel productModel = new ProductModel();
                    productModel.setId(product.getId());
                    productModel.setProductName(product.getProductName());
                    productModel.setColor(product.getColor());
                    productModel.setRam(product.getRam());
                    productModel.setUsp(product.getUsp());
                    productModel.setImageUrl(product.getImageUrl());
                    productModel.setQuantitySold(product.getQuantitySold());
                    productModel.setTotalStock(product.getTotalStock());
                    productModelList.add(productModel);
                }
                return productModelList;
            }
        } catch (Exception e) {
            log.error("Exceptiion Occured: ", e);
            return null;
        }
        return null;
    }

    public ResponseEntity<List<ProductModel>> getTopToSellingProducts() {
        ResponseEntity<List<ProductModel>> response = new ResponseEntity<>(HttpStatus.OK);
        log.info("Got request to get top selling products");
        List<ProductModel> productModelList = new ArrayList<>();
        try {
            List<ProductEntity> products = productRepository.findTop10ByOrderByQuantitySoldDesc();
            if (products != null && !products.isEmpty()) {
                for (ProductEntity product : products) {
                    ProductModel productModel = new ProductModel();
                    productModel.setId(product.getId());
                    productModel.setProductName(product.getProductName());
                    productModel.setColor(product.getColor());
                    productModel.setRam(product.getRam());
                    productModel.setUsp(product.getUsp());
                    productModel.setImageUrl(product.getImageUrl());
                    productModel.setQuantitySold(product.getQuantitySold());
                    productModel.setTotalStock(product.getTotalStock());
                    productModelList.add(productModel);
                }
                response = new ResponseEntity<>(productModelList, HttpStatus.OK);
                return response;
            }
        } catch (Exception e) {
            log.error("Exception Occurred: ", e);
            response = new ResponseEntity<>(productModelList, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    public  void updateStockOfProduct(String productId, Integer count) {
        log.info("Got call to update product stock by id: {}", productId);
        try {
            Optional<ProductEntity> productEntity = productRepository.findById(productId);
            if (productEntity.isPresent()) {
                Integer currentStock = productEntity.get().getTotalStock();
                log.info("Current count is: {}", currentStock);
                productEntity.get().setTotalStock(currentStock + count);
                productRepository.save(productEntity.get());
            }
        } catch (Exception e) {
            log.error("Exception occurred: ", e);
        }
    }

    public void deleteProductById(String productId) {
        log.info("Got call to delete product by Id: {}", productId);
        productRepository.deleteById(productId);
    }

    public ResponseEntity<String> addNewProduct(ProductModel productModel) {
        ResponseEntity<String> res = new ResponseEntity<>(HttpStatus.OK);
        log.info("Got product to add with data: {}", productModel);
        String productName = productModel.getProductName();
        String ram = productModel.getRam();
        String color = productModel.getColor();
        Boolean available = productRepository.existsByProductNameAndColorAndRam(productName, color, ram);
        if (!available) {
            log.info("Product available: {}", available);
            ProductEntity newProduct = new ProductEntity();
            newProduct.setProductName(productModel.getProductName());
            newProduct.setColor(productModel.getColor());
            newProduct.setRam(productModel.getRam());
            newProduct.setUsp(productModel.getUsp());
            newProduct.setImageUrl(productModel.getImageUrl());
            newProduct.setQuantitySold(productModel.getQuantitySold());
            newProduct.setTotalStock(productModel.getTotalStock());
            log.info("Create new product");
            ProductEntity newCreatedProduct = productRepository.save(newProduct);
            if (Objects.nonNull(newCreatedProduct)) {
                res = new ResponseEntity<>(newCreatedProduct.getId(), HttpStatus.OK);
                return res;
            }
        }
        else {
            log.info("Product exists going to get product id: ");
            Optional<ProductEntity> response = productRepository.findByProductNameAndColorAndRam(productModel.getProductName(),productModel.getRam(),productModel.getColor());
            log.info("Found Product: {}", response);
            if(response.isPresent()){
                log.info("Inside if: {}", response.get());
                updateStockOfProduct(response.get().getId(), productModel.getTotalStock());
                res = new ResponseEntity<>(response.get().getId(), HttpStatus.OK);
                return res;
            }
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
    public ResponseEntity<String> checkProduct(ProductSearchRequest productRequest){
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
        log.info("Inside check product got call to find product with detail: name: {}, color: {}, ram: {}", productRequest.getProductName(), productRequest.getColor(), productRequest.getRam());
        Optional<ProductEntity> res = productRepository.findByProductNameAndColorAndRam(productRequest.getProductName(),productRequest.getColor(),productRequest.getRam());
//        log.info("Db response to find product: {}", response.get());
        if(res.isPresent()){
            log.info("Found product details: {}", res.get());
            response = new ResponseEntity<>(res.get().getId(), HttpStatus.OK);
            return response;
        }
        log.info("Nothing Found");
       return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public ProductModel getProductById(String id){
        ProductModel productModel = null;
        Optional<ProductEntity> result = productRepository.findById(id);
        if (result.isPresent()){
            productModel = new ProductModel();
            productModel.setId(result.get().getId());
            productModel.setProductName(result.get().getProductName());
            productModel.setRam(result.get().getRam());
            productModel.setUsp(result.get().getUsp());
            productModel.setColor(result.get().getColor());
            productModel.setImageUrl(result.get().getImageUrl());
        }
        return productModel;
    }

    public ResponseEntity<List<ProductModel>> getProductByName(String productName) {
        ResponseEntity<List<ProductModel>> response = new ResponseEntity<>(HttpStatus.OK);
        log.info("Got call to get product by name: {}", productName);
        List<ProductModel> productModelList = new ArrayList<>();
        try {
            List<ProductEntity> products = productRepository.findByProductName(productName);
            if (products != null && !products.isEmpty()) {
                log.info("Got products from product repository: {}", products);
                for (ProductEntity product : products) {
                    ProductModel productModel = new ProductModel();
                    productModel.setId(product.getId());
                    productModel.setProductName(product.getProductName());
                    productModel.setColor(product.getColor());
                    productModel.setRam(product.getRam());
                    productModel.setUsp(product.getUsp());
                    productModel.setImageUrl(product.getImageUrl());
                    productModel.setQuantitySold(product.getQuantitySold());
                    productModel.setTotalStock(product.getTotalStock());
                    productModelList.add(productModel);
                }
                response = new ResponseEntity<>(productModelList, HttpStatus.OK);
                return response;
            }
        } catch (Exception e) {
            log.error("Exception Occurred: ", e);
            response =  new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }
}
